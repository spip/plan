<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/plan?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// C
	'confirmer_deplacement' => 'Ви впевнені, що хочете перемістити ці елементи??',

	// D
	'deplacement_en_echec' => 'Не вдалося виконати переміщення.',
	'deplacement_identifiant_absent' => 'Відсутні',
	'deplacement_reussi' => 'Переміщення виконано успішно.',
	'deplacements_en_echec' => '@nb@ переміщень не вдалося виконати.',
	'deplacements_reussis' => '@nb@ переміщено успішно.',

	// E
	'erreur_aucun_identifiant' => 'Відсутній ідентифікатор об’єкта.',
	'erreur_autorisation_insuffisante' => 'Недостатньо прав доступу.',
	'erreur_deplacement' => 'Переміщення не виконано.',
	'erreur_deplacement_impossible' => 'Переміщення неможливе!',
	'erreur_rubriques_parentes_incorrectes' => 'Батьківський розділ некоректний.',

	// P
	'plan_titre' => 'Карта сайту в адміністративній частині сайту',

	// S
	'suggerer_deplier' => 'Це може статися через переміщення великої групи: у такому разі список її дочірніх елементів не завантажується. Розгорніть блок (щоб завантажити його), згорніть назад і повторіть переміщення.',

	// T
	'tous_les_statuts' => 'Всі статуси',
];
