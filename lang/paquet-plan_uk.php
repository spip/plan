<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-plan?lang_cible=uk
// ** ne pas modifier le fichier **

return [

	// P
	'plan_description' => 'Цей плагін покращує сторінку «Карта сайту» в адміністративній частині сайту, додаючи деревоподібну структуру контекнту та можливість фільтрації за статусом статей.',
	'plan_nom' => 'Карта сайту в адміністративній частині сайту',
	'plan_slogan' => 'Покращує сторінку «Карта сайту» в адміністративній частині сайту',
];
