# Changelog

## 4.2.0 - 2025-11-25

### Added

- Installable en tant que package Composer
- Fichier `README.md`

### Changed

- Compatible SPIP 5.0.0-dev
